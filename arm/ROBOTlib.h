//
// Routines defined in ROBOTlib.c

void    turnLED(int) ;
int     fwd(float, float) ;
int     bwd(float, float) ;
int     right(void) ;
int     left(void) ;
int     cw(float, float) ;
int     ccw(float, float) ;
int     test_drive(void) ;
int     test_sonar(void) ;
int     test_servo(void) ;

